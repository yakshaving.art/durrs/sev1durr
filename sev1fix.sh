#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Simple sev-1 fixer
set -eufCo pipefail
export SHELLOPTS
IFS=$'\t\n'

command -v jq >/dev/null 2>/dev/null || { echo "Please install jq or use image that has it."; exit 42; }
command -v curl >/dev/null 2>/dev/null || { echo "Please install curl or use image that has it."; exit 42; }

if [[ "unset" == "${SLACK_TOKEN:-unset}" || ! "${SLACK_TOKEN}" =~ ^xoxs-.* ]]; then
	echo "Please export user token as SLACK_TOKEN env var"
	echo "To get user token, open web console, and run console.log(window.boot_data.api_token)"
	echo "No, legacy tokens won't do for emoji uploads"
	exit 1
fi

function _dump() {
	# dump all the emojis to $1
	local dir="${1:-./emojis}"
	if [[ ! -d "${dir}" ]]; then
		mkdir -p "${dir}" || { echo "failed"; return 42; }
	fi

	while IFS=' ' read -r name url; do
		echo "downloading '${name}.${url##*.}' from '${url}'"
		curl -qsSL --output "${dir}/${name}.${url##*.}" "${url}"
	done < <(curl -X GET \
			-H "Authorization: Bearer ${SLACK_TOKEN}" \
			https://slack.com/api/emoji.list \
			| jq -rc 'select(.ok==true) | .emoji | to_entries[] | select (.value | startswith("https://")) | [.key,.value] | join(" ")')
}

function _upload() {
	# uploads all emojis from $1 to slack
	local dir="${1:-./emojis}"
	while read -d $'\0' -r file; do
		name="$(basename "${file%%.*}")"
		echo -n "uploading emoji '${file}' as name '${name}': "
		curl -X POST \
			-H "Authorization: Bearer ${SLACK_TOKEN}" \
			-F "mode=data" \
			-F "name=${name}" \
			-F "image=@${file}" \
			https://slack.com/api/emoji.add
		echo
	done < <(find "${dir}" -type f -print0)
}

_upload "yak"	# for example
